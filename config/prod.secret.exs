use Mix.Config

# In this file, we keep production configuration that
# you likely want to automate and keep it away from
# your version control system.
config :hello, Hello.Endpoint,
  secret_key_base: "iJkc9/x4HE97UGfc6A9JqaThsnuOuaad/rIpKlWwImvmiBp2U1ORss/XeGCMD3jy"

# Configure your database
config :hello, Hello.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "hello_prod"
