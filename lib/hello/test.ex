defmodule Hello.TestWorker do
  import Plug.Conn, only: [get_req_header: 2]

  def start_link() do
    Task.async(fn -> 
      work_it()
    end)
    Agent.start_link(fn -> HashDict.new end, name: __MODULE__)
  end

  def get(key) do
    Agent.get(Hello.TestWorker, &HashDict.get(&1, key))
  end

  def increment() do
    %Hello.Ayy{xd: val} = 
      get("lmao") || %Hello.Ayy{xd: 0}
        
    update("lmao", val + 1)
    :ok
  end

  def update(key, value) do
    Agent.update(Hello.TestWorker, &HashDict.put(&1, key, %Hello.Ayy{xd: value}))
  end

  def get_context(conn) do
    IO.puts conn |> get_req_header("host")
    :ok
  end

  def work_it do
    :timer.sleep(1000)
    #IO.inspect Agent.get(Hello.TestWorker, &HashDict.get(&1, "lmao"))
    work_it
  end
end