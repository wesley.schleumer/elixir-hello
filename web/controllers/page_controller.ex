defmodule MotherFuckingMacros do
  import Hello.TestWorker

  defmacro defaction(name, do: body) do
    quote do
      def unquote(name)(var!(conn), var!(params)) do
        var!(context) = get_context(var!(conn))
        unquote(body)
      end
    end
  end
end

defmodule Hello.PageController do
  use Hello.Web, :controller
  import MotherFuckingMacros

  plug :action
  
  defaction :index do
    #render conn, "index.html"
    redirect conn, to: "/index.html"
  end
end
